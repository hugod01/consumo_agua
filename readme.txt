================== PROJETO CONSUMO DE AGUA EM CONDOMINIO ====================
============== SISTEMA FEITO POR HUGO DANIEL DESIDERIO RGM: 25976117 ========

1-) Requisitos para que o sistema funcione paginas extenção PHP
		--> Apache 
		--> Tomcat
		--> Banco de dados MySQL
		--> Ou sistema "XAMP", versão mais atualizada
		
2-) Importação de banco de dados
		--> Após instalação dos requisitos de infra acima:
		--> Criar o banco de "watermanagement"
		--> fazem a importação do arquivo "watermanagement.sql", que contem a estrtura de tabelas do sistema
		
3-) Baixar o arquivo de nome "consum_agua.zip" descompactar e fazer a copia para dentro da pasta "xampp/htdocs"

4-) Ajustar o arquivo"conexao.php" de conexão com banco de dados MySQL, que está no seguinte caminho "\xampp\htdocs\consum_agua\includes",
	apontando para o banco seja local ou remoto.
	
5-) Após o termino do passo acima abrir o seu browser e digite a seguinte url = "http://localhost/consum_agua/login/loginadm.php" para 	  acesso na mesma maquina, ou a url amigavel para acesso remoto.

6-) Na tabela de nome "administrador", contem um linha já criada com um dado de Administrador do sistema para acesso. 

7-) Após os passos acima o sistema está pronto para ser utilizado.

8-) Segue link de Vídeo Sistema de Consumo de Agua = https://youtu.be/T--xnWUA678
============================================================================================
