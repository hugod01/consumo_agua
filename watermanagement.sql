-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 07-Jan-2018 às 21:12
-- Versão do servidor: 10.1.21-MariaDB
-- PHP Version: 7.1.1

-- Your database has been setup. Use the following values:
-- Database:	sxvmszhw_watermanagement
-- Host:	localhost
-- https://x15.x10hosting.com:2222/user/database
-- https://x15.x10hosting.com/phpMyAdmin/index.php
-- Username:	sxvmszhw_watermanagement
-- Password:	Hdd290575@

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `watermanagement`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `admnistrador`
--

CREATE TABLE `admnistrador` (
  `id_adm` int(11) NOT NULL,
  `nm_adm` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `dt_cad_adm` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_cad_adm` varchar(255) NOT NULL,
  `login_adm` varchar(255) NOT NULL,
  `senha_adm` varchar(255) NOT NULL,
  `ADM` varchar(255) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `empresa` varchar(255) NOT NULL,
  `enrederço` varchar(255) NOT NULL,
  `dt_venc_boleto` varchar(30) NOT NULL,
  `telefone` varchar(50) NOT NULL,
  `contato` varchar(150) NOT NULL,
  `CNPJ` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `admnistrador`
--

INSERT INTO `admnistrador` (`id_adm`, `nm_adm`, `status`, `dt_cad_adm`, `user_cad_adm`, `login_adm`, `senha_adm`, `ADM`, `id_empresa`, `empresa`, `enrederço`, `dt_venc_boleto`, `telefone`, `contato`, `CNPJ`) VALUES
(1, 'Adiministrador Sistema', 'ATIVO', '2016-10-12 14:21:50', 'Adiministrador Sistema', 'adm_sistema', 'adm_sistema@123', 'ADM', 2, 'LELO CONDOMINIOS', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `condominio`
--

CREATE TABLE `condominio` (
  `id_cond` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `nm_cond` varchar(255) NOT NULL,
  `end_cond` varchar(255) NOT NULL,
  `cep_cond` varchar(255) NOT NULL,
  `estado_cond` varchar(255) NOT NULL,
  `pais_cond` varchar(255) NOT NULL,
  `site_cond` varchar(255) NOT NULL,
  `tel_cond` varchar(255) NOT NULL,
  `contato_cond` varchar(255) NOT NULL,
  `email_cto_cond` varchar(255) NOT NULL,
  `dt_cadastro_cond` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_cadastro` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `nm_zelador` varchar(255) NOT NULL,
  `tel_zelador` varchar(255) NOT NULL,
  `email_zelador` varchar(255) NOT NULL,
  `cel_zelador` varchar(255) NOT NULL,
  `cel_sindico` varchar(255) NOT NULL,
  `CNPJ_cond` varchar(200) NOT NULL,
  `valores` varchar(200) NOT NULL,
  `dt_vencimento` varchar(50) NOT NULL,
  `dia_prox_leitura` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Estrutura da tabela `cond_consumo`
--

CREATE TABLE `cond_consumo` (
  `id_consumo` int(11) NOT NULL,
  `id_cond` int(11) DEFAULT NULL,
  `mes_ref_consumo` varchar(255) DEFAULT NULL,
  `valor_conta_mes` varchar(255) DEFAULT NULL,
  `consumo_mes` varchar(255) DEFAULT NULL,
  `Coefici_mes` varchar(255) DEFAULT NULL,
  `perid_consumo` varchar(255) DEFAULT NULL,
  `proxima_leitura` varchar(255) DEFAULT NULL,
  `dt_cadastro` varchar(50) NOT NULL,
  `usuario_cadastro` varchar(255) DEFAULT NULL,
  `ano` varchar(50) DEFAULT NULL,
  `status_conta` varchar(255) DEFAULT NULL,
  `id_empresa` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `login_cont` varchar(255) NOT NULL,
  `assunto` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `dat_cont` varchar(50) NOT NULL,
  `id_cont` int(11) NOT NULL,
  `nome_cont` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `data_visu` varchar(50) NOT NULL,
  `usuario_visu` varchar(255) NOT NULL,
  `resposta` varchar(255) NOT NULL,
  `id_cond` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Estrutura da tabela `morador`
--

CREATE TABLE `morador` (
  `id_morador` int(11) NOT NULL,
  `id_cond` int(11) NOT NULL,
  `nm_morador` varchar(255) NOT NULL,
  `end_morador` varchar(255) NOT NULL,
  `num_cs_morador` int(255) NOT NULL,
  `compl_cs_morador` varchar(255) NOT NULL,
  `cep_morador` varchar(255) NOT NULL,
  `estado_morador` varchar(255) NOT NULL,
  `pais_morador` varchar(255) NOT NULL,
  `tel_morador` varchar(255) NOT NULL,
  `email_mordor` varchar(255) NOT NULL,
  `senha_morador` varchar(255) NOT NULL,
  `hidrometro` varchar(255) NOT NULL,
  `dt_ctro_morador` varchar(50) NOT NULL,
  `status` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `cel_morador` varchar(255) NOT NULL,
  `cel_morador1` varchar(255) NOT NULL,
  `nm_morador_anterior` varchar(255) NOT NULL,
  `valores` varchar(200) NOT NULL,
  `dt_venc` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Estrutura da tabela `morador_consumo`
--

CREATE TABLE `morador_consumo` (
  `id_cosum_mora` int(11) NOT NULL,
  `id_morador` int(11) NOT NULL,
  `mes_ref_consumo` varchar(255) NOT NULL,
  `lei_anterior` varchar(255) NOT NULL,
  `lei_atual` varchar(255) NOT NULL,
  `cosumo` varchar(255) NOT NULL,
  `vl_conta` varchar(255) NOT NULL,
  `dt_cadastro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_cadastro` varchar(255) NOT NULL,
  `ano` varchar(100) DEFAULT NULL,
  `status_conta` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Indexes for dumped tables
--

--
-- Indexes for table `admnistrador`
--
ALTER TABLE `admnistrador`
  ADD PRIMARY KEY (`id_adm`);

--
-- Indexes for table `condominio`
--
ALTER TABLE `condominio`
  ADD PRIMARY KEY (`id_cond`);

--
-- Indexes for table `cond_consumo`
--
ALTER TABLE `cond_consumo`
  ADD PRIMARY KEY (`id_consumo`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id_cont`);

--
-- Indexes for table `morador`
--
ALTER TABLE `morador`
  ADD PRIMARY KEY (`id_morador`);

--
-- Indexes for table `morador_consumo`
--
ALTER TABLE `morador_consumo`
  ADD PRIMARY KEY (`id_cosum_mora`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admnistrador`
--
ALTER TABLE `admnistrador`
  MODIFY `id_adm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `condominio`
--
ALTER TABLE `condominio`
  MODIFY `id_cond` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `cond_consumo`
--
ALTER TABLE `cond_consumo`
  MODIFY `id_consumo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1223;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id_cont` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `morador`
--
ALTER TABLE `morador`
  MODIFY `id_morador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `morador_consumo`
--
ALTER TABLE `morador_consumo`
  MODIFY `id_cosum_mora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=416;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
